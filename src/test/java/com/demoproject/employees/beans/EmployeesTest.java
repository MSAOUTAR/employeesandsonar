package com.demoproject.employees.beans;

import static org.junit.Assert.*;

import org.junit.Test;

public class EmployeesTest {

	@Test
	public void testSetId() {
		Employees employees = new Employees();
		employees.setId(1);
        assertTrue(employees.getId() == 1);
	}


	@Test
	public void testSetName() {
		Employees employees = new Employees();
		employees.setName("marouane");
        assertTrue(employees.getName().equals("marouane"));
	}


	@Test
	public void testSetAge() {
		Employees employees = new Employees();
		employees.setAge(18);
        assertTrue(employees.getAge() == 18);
	}


	@Test
	public void testSetRole() {
		Employees employees = new Employees();
		employees.setRole("Director");
        assertTrue(employees.getRole().equals("Director"));
	}

}
