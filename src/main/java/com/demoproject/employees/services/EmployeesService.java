package com.demoproject.employees.services;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Service;

import com.demoproject.employees.beans.Employees;

@Service
public class EmployeesService {
	
	public List<Employees> returnAllEmployees(){
		
		List<Employees> listEmployees = new ArrayList<>();
		listEmployees.add(new Employees(1, "James", 40, "Manager"));
		listEmployees.add(new Employees(2, "David", 30, "Developer"));
		listEmployees.add(new Employees(3, "Mano", 40, "HR"));
		
		return listEmployees;
	}

}
