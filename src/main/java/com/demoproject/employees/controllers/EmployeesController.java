package com.demoproject.employees.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import com.demoproject.employees.beans.Employees;
import com.demoproject.employees.services.EmployeesService;

@RestController
public class EmployeesController {
	
	@Autowired
	EmployeesService employeesService;
	
	@GetMapping("/employees")
	public List<Employees> recieveProducts(){

			return employeesService.returnAllEmployees();		
	}

}
