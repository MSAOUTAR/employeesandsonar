FROM adoptopenjdk/openjdk8:alpine
MAINTAINER marouane
ARG JAR_FILE=target/employees.jar
WORKDIR /opt/app
COPY ${JAR_FILE} app.jar
EXPOSE 8086
ENTRYPOINT ["java","-jar","app.jar"]
